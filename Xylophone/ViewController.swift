//
//  ViewController.swift
//  Xylophone
//
//  Created by Gerson  on 04/08/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVAudioPlayerDelegate {

    var audioPlayer: AVAudioPlayer?
    let soundArray = ["note1", "note2", "note3", "note4", "note5", "note6", "note7"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func notePressed(_ sender: UIButton) {
        var selectedSoundFileName: String = soundArray[sender.tag - 1]
        playSound(sound: selectedSoundFileName)
    }
    
    func playSound(sound: String) {
        let soundUrl = Bundle.main.url(forResource: sound, withExtension: "wav")
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
        } catch {
            print(error)
        }
        audioPlayer?.play()
    }
}

